import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:snap/ui/pages/camera.dart';
import 'package:snap/ui/pages/simulatorError.dart';
import './ui/pages/account.dart';
import './ui/pages/map.dart';
import './ui/pages/auth.dart';
import 'package:flutter/cupertino.dart';
import './ui/pages/chatList.dart';

var cameras = null;
var firstCamera = null;
bool cameraFail = false;

Future<void> main() async{
  WidgetsFlutterBinding.ensureInitialized();


  try{
    cameras = await availableCameras();
    firstCamera = cameras.first;
  }catch(e) {
    cameraFail = true;
  }

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.grey,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;


  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with SingleTickerProviderStateMixin{
  int _activeIndex = 0;

  String title = "BottomNavigationbar";
  late TabController _tabController;



  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 4, vsync: this);
  }


  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _tabController.addListener(() {
      if (_tabController.indexIsChanging) {
        setState(() {
          _activeIndex = _tabController.index;
        });
      }
    });
    return Scaffold(
        body:
        TabBarView(
          children: <Widget>[
            Account(),
            cameraFail ? SimError() :
            TakePictureScreen(
              // Pass the appropriate camera to the TakePictureScreen widget.
              camera: firstCamera,
            ),
            ChatList(),
            Map(),
          ],
          controller: _tabController,
        ),
        bottomNavigationBar: Container(
            padding: EdgeInsets.all(16.0),
            child : ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(50.0),),
                child : Container(
                  color: Colors.black,
                  child : TabBar(
                    labelColor: Colors.white,
                    labelStyle: TextStyle(fontSize: 10.0),
                    unselectedLabelColor: Colors.grey,
                    tabs: <Widget>[
                      Tab(
                        icon: SvgPicture.asset(
                            "assets/profile.svg",
                            color: _activeIndex == 0 ? Colors.white : Colors.grey,
                            semanticsLabel: 'Profile',
                            width: 30,
                            height: 30,
                        ),
                      ),
                      Tab(
                        icon: SvgPicture.asset(
                          "assets/camera.svg",
                          color: _activeIndex == 1 ? Colors.white : Colors.grey,
                          semanticsLabel: 'Camera',
                          width: 20,
                          height: 20,
                        ),
                      ),
                      Tab(
                        icon: SvgPicture.asset(
                          "assets/message.svg",
                          color: _activeIndex == 2 ? Colors.white : Colors.grey,
                          semanticsLabel: 'Message',
                          width: 25,
                          height: 25,
                        ),
                      ),
                      Tab(
                        icon: SvgPicture.asset(
                          "assets/map.svg",
                          color: _activeIndex == 3 ? Colors.white : Colors.grey,
                          semanticsLabel: 'Map',
                          width: 25,
                          height: 25,
                        ),
                      ),
                    ],
                    controller: _tabController,
                  ),
                )
            )
        )
    );
  }
}
