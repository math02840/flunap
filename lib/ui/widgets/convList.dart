import 'package:flutter/material.dart';
import '../pages/chatPage.dart';

class ConversationList extends StatefulWidget{
  String name;
  String status;
  String imageUrl;
  String time;
  bool isMessageRead;
  ConversationList({required this.name, required this.status, required this.imageUrl, required this.time, required this.isMessageRead});
  @override
  _ConversationListState createState() => _ConversationListState();
}

class _ConversationListState extends State<ConversationList> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Navigator.push(context, MaterialPageRoute(builder: (context){
          return ChatPage(userInfo: widget);
        }));
      },
      child: Container(
        padding: EdgeInsets.only(left: 8,right: 16,top: 5,bottom: 5),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Row(
                children: <Widget>[

                  SizedBox(width: 16,),
                  Expanded(
                    child: Container(
                      color: Colors.transparent,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(widget.name, style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
                          SizedBox(height: 6,),
                          Row(
                            children : <Widget>[
                              Text(widget.status,style: TextStyle(fontSize: 14,fontWeight: widget.isMessageRead?FontWeight.bold:FontWeight.normal),),
                              Text(" - "+widget.time,style: TextStyle(fontSize: 14,),),

                            ]
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            CircleAvatar(
              backgroundImage: AssetImage(widget.imageUrl),
              maxRadius: 30,
            ),
          ],
        ),
      ),
    );
  }
}