import 'package:flutter/material.dart';

import '../../main.dart';
import 'connexion.dart';

class SimError extends StatelessWidget {
  const SimError({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SafeArea(
              child: Padding(
                padding: EdgeInsets.only(left: 16,right: 0,top: 10, bottom: 0),
                child:
                    Text("L'appareil photo n'est pas disponible sur le simulateur. Essayez sur un autre simulateur (Chrome) ou sur un appareil physique.",style: TextStyle(fontSize: 15,)
              ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
