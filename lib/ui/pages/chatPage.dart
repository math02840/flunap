import 'package:flutter/material.dart';
import '../widgets/convList.dart';
import '../models/chatModel.dart';

class ChatPage extends StatefulWidget {

  const ChatPage({super.key, required this.userInfo});

  final ConversationList userInfo;

  ConversationList get infos {
    return userInfo;
  }

  @override
  _ChatPageState createState() => _ChatPageState();

}

class _ChatPageState extends State<ChatPage> {

  //Hard Coded Messages
  List<ChatMessage> messages = [
    ChatMessage(messageContent: "Salut ! ça va ?", messageType: true),
    ChatMessage(messageContent: "oui et toi ?", messageType: false),
    ChatMessage(messageContent: "non", messageType: true),
    ChatMessage(messageContent: "Ok.", messageType: false),
  ];


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          automaticallyImplyLeading: false,
          backgroundColor: Colors.grey[100],
          flexibleSpace: SafeArea(
            child: Container(
              padding: EdgeInsets.only(right: 16),
              child: Row(
                children: <Widget>[
                  IconButton(
                    onPressed: (){
                      Navigator.pop(context);
                    },
                    icon: Icon(Icons.arrow_back,color: Colors.black,),
                  ),
                  SizedBox(width: 2,),
                  CircleAvatar(
                    backgroundImage: AssetImage(widget.infos.imageUrl),
                    maxRadius: 20,
                  ),
                  SizedBox(width: 12,),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(widget.infos.name,style: TextStyle( fontSize: 16 ,fontWeight: FontWeight.w600),),
                      ],
                    ),
                  ),

                ],
              ),
            ),
          ),
        ),
        body: Stack(

          children: <Widget>[
            ListView.builder(
              itemCount: messages.length,
              shrinkWrap: true,
              padding: EdgeInsets.only(top: 10,bottom: 10),
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (context, index){
                return Container(
                  padding: EdgeInsets.only(left: 14,right: 14,top: 10,bottom: 10),
                  child: Align(
                    alignment: (messages[index].messageType?Alignment.topLeft:Alignment.topRight),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: (messages[index].messageType?Colors.grey.shade200:Colors.grey[400]),
                      ),
                      padding: EdgeInsets.all(16),
                      child: Text(messages[index].messageContent, style: TextStyle(fontSize: 15),),
                    ),
                  ),
                );
              },
            ),
            Align(
              alignment: Alignment.bottomLeft,
              child: Container(
                padding: EdgeInsets.only(left: 15,right:10, bottom: 20,top: 10),
                height: 90,
                  color: Colors.white,

                  width: double.infinity,
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(25),

                  ),
                  padding: EdgeInsets.only(left: 5,right:5, bottom: 10,top: 10),
                  child: Row(
                    children: <Widget>[
                      SizedBox(width: 15,),
                      Expanded(
                        child: TextField(
                          decoration: InputDecoration(
                              hintText: "Écrire un message...",
                              hintStyle: TextStyle(color: Colors.black54),
                              border: InputBorder.none
                          ),
                        ),
                      ),
                      SizedBox(width: 15,),
                      FloatingActionButton(
                        onPressed: (){},
                        child: Icon(Icons.send,color: Colors.white,size: 18,),
                        backgroundColor: Colors.black,
                        elevation: 0,
                      ),
                    ],

                  ),
                )


              ),
            ),
          ],
        ),
    );
  }
}