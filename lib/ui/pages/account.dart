import 'package:flutter/material.dart';
import '../pages/connexion.dart';

class Account extends StatelessWidget {
  const Account({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SafeArea(
              child: Padding(
                padding: EdgeInsets.only(left: 16,right: 16,top: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Mon profil",style: TextStyle(fontSize: 32,fontWeight: FontWeight.bold),),
                  ],
                ),
              ),
            ),
            ListView(
              children: [
                Container(
                  child: Text(
                    "Profil",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
                  ),
                  margin: const EdgeInsets.only(top: 25, bottom: 20, left: 10),
                ),
                Container(
                  margin: EdgeInsets.only(left: 20, bottom: 20),
                  child: Row(
                    children: [
                      CircleAvatar(
                        backgroundImage: AssetImage('assets/image.jpeg'),
                        radius: 30,
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Marge S.",
                              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                            ),
                            Text("user1@efficom-lille.com"),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Card(
                  child: ListTile(
                    title: Text("Compte(s) bloqué(s)"),
                  ),
                ),
                GestureDetector(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context){
                  return Connexion();
                  }));
                  },
                  child: Card(
                    child: ListTile(
                    title: Text("Déconnexion"),
                    )
                    ),
                ),

              ],
              padding: EdgeInsets.all(10),
              shrinkWrap: true,
            )
          ],
        ),
      ),
    );
  }
}
