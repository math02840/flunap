import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import '../models/userModel.dart';
import '../widgets/convList.dart';
import '../models/messageStatus.dart';
import '../pages/account.dart';


class ChatList extends StatefulWidget {
  @override
  _ChatListState createState() => _ChatListState();
}

class _ChatListState extends State<ChatList> {
  @override

  //Hard Coded Users
  List<ChatUsers> chatUsers = [
    ChatUsers(name: "Oncle K", status: MessageStatus.getValue(Status.NEW), imageURL: "assets/image_1.jpeg", time: "À l'instant"),
    ChatUsers(name: "Paul V", status: MessageStatus.getValue(Status.OPEN), imageURL: "assets/image_2.jpeg", time: "15 minutes"),
    ChatUsers(name: "BBQ Mayo", status: MessageStatus.getValue(Status.READ), imageURL: "assets/image_3.jpeg", time: "31 Mai"),
    ChatUsers(name: "le Z", status: MessageStatus.getValue(Status.OPEN), imageURL: "assets/image_4.jpeg", time: "2 heures"),
    ChatUsers(name: "Noël Flantier", status: MessageStatus.getValue(Status.READ), imageURL: "assets/image_5.jpeg", time: "23 Mars"),
    ChatUsers(name: "Jean", status: MessageStatus.getValue(Status.OPEN), imageURL: "assets/image_6.jpeg", time: "1 mois"),
    ChatUsers(name: "Kaaris", status: MessageStatus.getValue(Status.READ), imageURL: "assets/image_7.jpeg", time: "24 Février"),
    ChatUsers(name: "Boris", status: MessageStatus.getValue(Status.READ), imageURL: "assets/image_1.jpeg", time: "18 Février"),
  ];

  Widget build(BuildContext context) {

    return Scaffold(
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),

        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SafeArea(
              child: Padding(
                padding: EdgeInsets.only(left: 16,right: 8,top: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Contact",style: TextStyle(fontSize: 32,fontWeight: FontWeight.bold),),
                    Container(
                      padding: EdgeInsets.only(left: 0,right: 0,top: 2,bottom: 2),
                      height: 50,
                      child:  Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                      Container(
                      color: Colors.white,
                          child: Image.asset(
                            'assets/add_contact.png',
                            fit: BoxFit.contain,
                            height: 100,

                          ),

                      ),
                             GestureDetector(
                                  onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context){
                            return Account();
                            }));
                            },
        child:CircleAvatar(
          backgroundImage: AssetImage('assets/image.jpeg'),
          radius: 30,
        ),
            ),


                            ])
                    )

                  ],
                ),
              ),
            ),
            ListView.separated(
              separatorBuilder: (context, index) => Divider(
                color: Colors.grey[500],
              ),
              itemCount: chatUsers.length,
              shrinkWrap: true,
              padding: EdgeInsets.only(top: 16),
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (context, index){
                return ConversationList(
                  name: chatUsers[index].name,
                  status: chatUsers[index].status,
                  imageUrl: chatUsers[index].imageURL,
                  time: chatUsers[index].time,
                  isMessageRead: chatUsers[index].status==MessageStatus.getValue(Status.NEW)?true:false,
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}