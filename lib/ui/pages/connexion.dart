import 'package:flutter/material.dart';

import '../../main.dart';
import '../pages/auth.dart';

class Connexion extends StatelessWidget {
  const Connexion({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SafeArea(
              child: Padding(
                padding: EdgeInsets.only(left: 16,right: 16,top: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text("Connexion",style: TextStyle(fontSize: 32,fontWeight: FontWeight.bold),),

                  ],
                ),
              ),
            ),
            SafeArea(
              child: Padding(
                padding: EdgeInsets.only(left: 16,right: 0,top: 0, bottom: 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text("Connectez vous à votre compte",style: TextStyle(fontSize: 20,),
                    ),
                  ],
                ),
              ),
            ),

            Container (
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[

                  Padding(
                    padding: EdgeInsets.only(top: 5, bottom: 5),
                    child: TextField(
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        hintText: 'Email',
                        hintStyle: TextStyle(fontSize: 16),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(50),
                          borderSide: BorderSide(
                            width: 0,
                            style: BorderStyle.none,
                          ),
                        ),
                        filled: true,
                        contentPadding: EdgeInsets.all(16),
                      ),
                    ), //Container
                  ),

                  Padding(
                    padding: EdgeInsets.only(top: 5, bottom: 5),
                    child: TextField(
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        hintText: 'Mot de passe',
                        hintStyle: TextStyle(fontSize: 16),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(50),
                          borderSide: BorderSide(
                            width: 0,
                            style: BorderStyle.none,
                          ),
                        ),
                        filled: true,
                        contentPadding: EdgeInsets.all(16),
                      ),
                    ), //Container
                  ),
                  Center(
                    child:
                      Padding(
                          padding: EdgeInsets.only(top: 30, bottom: 30),
                        child: TextButton(
                          child: Padding(
                            padding: const EdgeInsets.all(16),
                            child: Text('Connexion',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500)),
                          ),
                          style: ElevatedButton.styleFrom(
                            primary: Colors.black,
                            onPrimary: Colors.white,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(32.0),
                            ),
                          ),
                          onPressed: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context){
                              return MyHomePage(title: "home");
                            }));
                          },
                        ),
                      )
                  ),

    GestureDetector(
    onTap: (){
    Navigator.push(context, MaterialPageRoute(builder: (context){
    return Auth();
    }));
    },
    child:
                  Center(
                    child: Text.rich(
                        TextSpan(
                        text: 'Pas encore de compte ? ',
                        style: TextStyle(fontSize: 15),
                        children: <TextSpan>[
                        TextSpan(
                        text: 'Inscrivez-vous',
                        style: TextStyle(
                        decoration: TextDecoration.underline,
                        )),
                        // can add more TextSpans here...
                        ],
                        ),
                        )
                    ),
    ),

                  //Padding
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
