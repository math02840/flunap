import 'package:flutter/cupertino.dart';

class ChatMessage{
  String messageContent;
  bool messageType;
  // true for reciever
  ChatMessage({required this.messageContent, required this.messageType});
}