import 'package:flutter/cupertino.dart';

class ChatUsers{
  String name;
  String status;
  String imageURL;
  String time;
  ChatUsers({required this.name, required this.status, required this.imageURL, required this.time});
}