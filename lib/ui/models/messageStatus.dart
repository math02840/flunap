enum Status {
  SEND,
  READ,
  OPEN,
  NEW;

}

class MessageStatus{

  static String getValue(Status status){
    switch(status){
      case Status.SEND:
        return "Envoyé";
      case Status.READ:
        return "Reçu";
      case Status.OPEN:
        return "Ouvert";
      case Status.NEW:
        return "Nouveau";
      default:
        return "";
    }
  }

}